$(document).ready(function () {
  // var socket = io.connect('wss://' + location.host); //enter the IP of your beaglebone and port you are using in app.js

  var data_arr_fifteen_sec = [];
  var time_arr_fifteen_sec = [];

  var data_arr_min = [];
  var time_arr_min = [];

  var data_arr_five_min = [];
  var time_arr_five_min = [];

  var data_arr_fifteen_min = [];
  var time_arr_fifteen_min = [];

  var timeFormat = 'HH:mm:ss';
  var chartColor = '#FFFFFF';
  var count = '0';
  var data_value = '';
  var time_value = '';

  var ws = new WebSocket('wss://' + location.host);

  ws.onopen = function () {
    console.log('Successfully connect WebSocket');
  }

  ws.onmessage = function (message) {
    try {
      var obj = JSON.parse(message.data);
      if (obj) {

        data_value = obj.temperature;
        time_value = obj.time;
        // time_value = obj.time.toLocaleTimeString();

        data_arr_fifteen_sec.push(data_value);
        time_arr_fifteen_sec.push(time_value);

        if ((count == 0) || (count == 3) || (count == 7) || (count == 11) || (count == 15) || (count == 19) ||
          (count == 23) || (count == 27) || (count == 31) || (count == 35) || (count == 39) ||
          (count == 43) || (count == 47) || (count == 51) || (count == 55) || (count == 59)
        ) {
          data_arr_min.push(data_value);
          time_arr_min.push(time_value);
        }

        if ((count == 0) || (count == 19) || (count == 39) || (count == 59)) {
          data_arr_five_min.push(data_value);
          time_arr_five_min.push(time_value);
        }

        if ((count == 59) || (count == 0)) {
          data_arr_fifteen_min.push(data_value);
          time_arr_fifteen_min.push(time_value);
          if (count == 59) {
            count = '0';
          }
        }

        count++;

        if (data_arr_fifteen_sec.length >= 25) {
          data_arr_fifteen_sec.splice(0, 1);
          time_arr_fifteen_sec.splice(0, 1);
        }

        if (data_arr_min.length >= 15) {
          data_arr_min.splice(0, 1);
          time_arr_min.splice(0, 1);
        }

        if (data_arr_five_min.length >= 15) {
          data_arr_five_min.splice(0, 1);
          time_arr_five_min.splice(0, 1);
        }

        if (data_arr_fifteen_min.length >= 15) {
          data_arr_fifteen_min.splice(0, 1);
          time_arr_fifteen_min.splice(0, 1);
        }

        if (data_arr_fifteen_sec.length === time_arr_fifteen_sec.length) {
          window.myLine_fifteen_sec.update();
        }

        if (data_arr_min.length === time_arr_min.length) {
          window.myLine_min.update();
        }

        if (data_arr_five_min.length === time_arr_five_min.length) {
          window.myLine_five_min.update();
        }
      }
    } catch (err) {
      console.error(err);
    }

  };

  gradientChartOptionsConfiguration_fifteen_sec = {
    title: {
      display: true,
      text: 'Room Temperature Every 15 sec'
    },
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      bodySpacing: 4,
      mode: "nearest",
      intersect: 0,
      position: "nearest",
      xPadding: 10,
      yPadding: 10,
      caretPadding: 10
    },
    responsive: 1,
    scales: {
      yAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          drawTicks: true,
          display: true,
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'Temperature'
        },
        ticks: {
          // beginAtZero: true
          suggestedMin: 26,
          // max: 45
          suggestedMax: 36
        }
      }],
      xAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          display: false,
        },
        type: 'time',
        time: {
          format: timeFormat,
          tooltipFormat: 'll HH:mm:ss'
        },
        scaleLabel: {
          display: true,
          labelString: 'Time'
        }
      }]
    },
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 15,
        bottom: 15
      }
    }
  };

  gradientChartOptionsConfiguration_min = {
    title: {
      display: true,
      text: 'Room Temperature Every minute'
    },
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      bodySpacing: 4,
      mode: "nearest",
      intersect: 0,
      position: "nearest",
      xPadding: 10,
      yPadding: 10,
      caretPadding: 10
    },
    responsive: 1,
    scales: {
      yAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          drawTicks: true,
          display: true,
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'Temperature'
        },
        ticks: {
          // beginAtZero: true
          suggestedMin: 26,
          // max: 45
          suggestedMax: 36
        }
      }],
      xAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          display: false,
        },
        type: 'time',
        time: {
          format: timeFormat,
          tooltipFormat: 'll HH:mm:ss'
        },
        scaleLabel: {
          display: true,
          labelString: 'Time'
        }
      }]
    },
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 15,
        bottom: 15
      }
    }
  };

  gradientChartOptionsConfiguration_five_min = {
    title: {
      display: true,
      text: 'Room Temperature Every 5 minute'
    },
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    tooltips: {
      bodySpacing: 4,
      mode: "nearest",
      intersect: 0,
      position: "nearest",
      xPadding: 10,
      yPadding: 10,
      caretPadding: 10
    },
    responsive: 1,
    scales: {
      yAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          drawTicks: true,
          display: true,
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'Temperature'
        },
        ticks: {
          // beginAtZero: true
          suggestedMin: 26,
          // max: 45
          suggestedMax: 36
        }
      }],
      xAxes: [{
        gridLines: {
          zeroLineColor: "transparent",
          display: false,
        },
        type: 'time',
        time: {
          format: timeFormat,
          tooltipFormat: 'll HH:mm'
        },
        scaleLabel: {
          display: true,
          labelString: 'Time'
        }
      }]
    },
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 15,
        bottom: 15
      }
    }
  };

  var ctx_fifteen_sec = document.getElementById('canvas_fifteen_sec').getContext('2d');
  var ctx_min = document.getElementById('canvas_min').getContext('2d');
  var ctx_five_min = document.getElementById('canvas_five_min').getContext('2d');

  gradientFill_fifteen_sec = ctx_fifteen_sec.createLinearGradient(0, 170, 0, 50);
  gradientFill_fifteen_sec.addColorStop(0, "rgba(128, 182, 244, 0)");
  gradientFill_fifteen_sec.addColorStop(1, "rgba(249, 99, 59, 0.40)");

  gradientFill_min = ctx_min.createLinearGradient(0, 170, 0, 50);
  gradientFill_min.addColorStop(0, "rgba(128, 182, 244, 0)");
  gradientFill_min.addColorStop(1, "rgba(249, 99, 59, 0.40)");

  gradientFill_five_min = ctx_five_min.createLinearGradient(0, 170, 0, 50);
  gradientFill_five_min.addColorStop(0, "rgba(128, 182, 244, 0)");
  gradientFill_five_min.addColorStop(1, "rgba(249, 99, 59, 0.40)");

  myChart_fifteen_sec = new Chart(ctx_fifteen_sec, {
    type: 'line',
    data: {
      labels: time_arr_fifteen_sec,
      datasets: [{
        borderColor: "#f96332",
        pointBorderColor: "#FFF",
        pointBackgroundColor: "#f96332",
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 1,
        pointRadius: 4,
        fill: true,
        backgroundColor: gradientFill_fifteen_sec,
        borderWidth: 2,
        data: data_arr_fifteen_sec
      }]
    },
    options: gradientChartOptionsConfiguration_fifteen_sec
  });

  myChart_min = new Chart(ctx_min, {
    type: 'line',
    data: {
      labels: time_arr_min,
      datasets: [{
        borderColor: "#f96332",
        pointBorderColor: "#FFF",
        pointBackgroundColor: "#f96332",
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 1,
        pointRadius: 4,
        fill: true,
        backgroundColor: gradientFill_min,
        borderWidth: 2,
        data: data_arr_min
      }]
    },
    options: gradientChartOptionsConfiguration_min
  });

  myChart_five_min = new Chart(ctx_five_min, {
    type: 'line',
    data: {
      labels: time_arr_five_min,
      datasets: [{
        borderColor: "#f96332",
        pointBorderColor: "#FFF",
        pointBackgroundColor: "#f96332",
        pointBorderWidth: 2,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 1,
        pointRadius: 4,
        fill: true,
        backgroundColor: gradientFill_five_min,
        borderWidth: 2,
        data: data_arr_five_min
      }]
    },
    options: gradientChartOptionsConfiguration_five_min
  });

  window.onload = function () {
    window.myLine_fifteen_sec = myChart_fifteen_sec;
    window.myLine_min = myChart_min;
    window.myLine_five_min = myChart_five_min;
  };

});